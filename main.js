window.addEventListener("load", function(){
  const canvas = document.getElementById('canvas1');
  const ctx = canvas.getContext('2d');
  canvas.width = 1280;
  canvas.height = 720;

  ctx.fillStyle = 'white';
  ctx.lineWidth = 3;
  ctx.strokeStyle = 'black';
  ctx.font = '40px Helvetica';
  ctx.textAlign = 'center';

  class Player {
    constructor(game){
      this.game = game;
      this.collisionX = this.game.width * 0.5; // collisionX & collisionY define the position of collion cercle
      this.collisionY = this.game.height * 0.5;
      this.collisionRadius = 30;
      this.speedX = 0;
      this.speedY = 0;
      this.dx = 0;
      this.dy = 0;
      this.speedModifier = 3;
      this.spriteWidth = 255;
      this.spriteHeight = 256;
      this.width = this.spriteWidth; // width & height: to allow the potential for scaling the player up & down (if we want later maybe)
      this.height = this.spriteHeight;
// spriteX & spriteY define the top left corner of sprite sheet frame image we are currently drawing to represent the player
      this.spriteX;
      this.spriteY;
      this.frameX = 1; // horizontal sprite navigation
      this.frameY = 5; // vertical sprite nag.
      this.image = document.getElementById('bull');
    }
    draw(context){
// context image takes 3 arguments: image, x & y where to draw it
      context.drawImage(
        this.image, //sx, sy, sw, sh, // this 4 argurments are corresponding to :
        this.frameX * this.spriteWidth, 
        this.frameY * this.spriteHeight, 
        this.spriteWidth, 
        this.spriteHeight,
        this.spriteX, this.spriteY, // this.collisionX, this.collisionY,
        this.width, this.height);
      // debug mode on player
      if (this.game.debug){
        context.beginPath();
        context.arc(this.collisionX, this.collisionY, this.collisionRadius, 0, Math.PI * 2);
        context.save();
        context.globalAlpha = 0.5;
        context.fill();
        context.restore();
        context.stroke();
        context.beginPath();
        context.moveTo(this.collisionX, this.collisionY);
        context.lineTo(this.game.mouse.x, this.game.mouse.y);
        context.stroke();
      }
    }
    update(){      
      this.dx = this.game.mouse.x - this.collisionX;
      this.dy = this.game.mouse.y - this.collisionY;
      /**
       * SPRITE ANIMATION: 
       * use "Math.atan2()" that returns an angle in radians 
       * between the positive x axis & a line, 
       * projected from 0, 0 towards a specific point
       * 
       * For that player always facing in direction it is move in, 
       * to work the mouse cursor
       */
      const angle = Math.atan2(this.dy, this.dx);
      if (angle < -2.74 || angle > 2.74) this.frameY = 6;
      else if (angle < -1.96) this.frameY = 7;
      else if (angle < -1.17) this.frameY = 0;
      else if (angle < -0.39) this.frameY = 1;
      else if (angle < 0.39) this.frameY = 2;
      else if (angle < 1.17) this.frameY = 3;
      else if (angle < 1.96) this.frameY = 4;
      else if (angle < 2.74) this.frameY = 5;
      
      //console.log(angle);

      const distance = Math.hypot(this.dy, this.dx);
      if (distance > this.speedModifier){
        this.speedX = this.dx/distance || 0;
        this.speedY = this.dy/distance || 0;
      } else {
        this.speedX = 0;
        this.speedY = 0;
      }
      
      this.collisionX += this.speedX * this.speedModifier;
      this.collisionY += this.speedY * this.speedModifier;
      this.spriteX = this.collisionX - this.width * 0.5;
      this.spriteY = this.collisionY - this.height * 0.5 - 100;
      // Horizontal player movement boundaries
      if (this.collisionX < this.collisionRadius) 
      this.collisionX = this.collisionRadius;
      else if (this.collisionX > this.game.width - this.collisionRadius) 
      this.collisionX = this.game.width - this.collisionRadius;
      // Vertical player movement boundaries
      if (this.collisionY < this.game.topMagin + this.collisionRadius) 
      this.collisionY = this.game.topMagin + this.collisionRadius;
      else if (this.collisionY > this.game.height - this.collisionRadius) 
      this.collisionY = this.game.height - this.collisionRadius;
      // collisions with obstacles
      this.game.obstacles.forEach(obstacle => {
        // helper ref. : [(distance < sumOfRadii), distance, sumOfRadii, dx,  dy]; 
        // use detructuring
        let [collision , distance, sumOfRadii, dx,  dy] = 
        this.game.checkCollision(this, obstacle); 
// to push the player 1px outside the collition radius of the obstacle, 
// in the direction away of the center point        
        if(collision){
          // console.log('crash');
          const unit_x = dx / distance;
          const unit_y = dy / distance;
          // console.log(unit_x, unit_y);
          this.collisionX = obstacle.collisionX + (sumOfRadii + 1) * unit_x;
          this.collisionY = obstacle.collisionY + (sumOfRadii + 1) * unit_y;
        }
      })
    }
  }  

  class Obstacle {
    constructor(game){ 
      this.game = game
      this.collisionX = Math.random() * this.game.width; // all this values will be access directly by this "Obstacle" class
      this.collisionY = Math.random() * this.game.height;
      this.collisionRadius = 40;
      this.image = document.getElementById("obstacles");
      this.spriteWidth = 250;
      this.spriteHeight = 250;
      this.width = this.spriteWidth;
      this.height = this.spriteHeight;
      this.spriteX = this.collisionX - this.width * 0.5;
      this.spriteY = this.collisionY - this.height * 0.5 - 70;
      // determ. witch col. is choose on img obstacles spriteSheet
      this.frameX = Math.floor(Math.random() * 4);
      this.frameY = Math.floor(Math.random() * 3);
    }
    draw(context){
      // context.drawImage(this.image, sx, sy, sw, sh, this.collisionX, this.collisionY, this.width, this.height);
      // to divide obstacles image & take only one obstacle (top left) & center this one in obstacle cirle just created before 
      context.drawImage(
        this.image, 
        this.frameX * this.spriteWidth, 
        this.frameY * this.spriteHeight, 
        // 0 * this.spriteHeight, 
        this.spriteWidth, 
        this.spriteHeight, 
        this.spriteX, 
        this.spriteY, 
        this.width, 
        this.height
        );
      if (this.game.debug){
        context.beginPath();
        context.arc(this.collisionX, this.collisionY, this.collisionRadius, 0, Math.PI * 2);
        context.save();
        context.globalAlpha = 0.5;
        context.fill();
        context.restore();
        context.stroke();
      }
    }
    update(){

    }
  }
  
  class Egg {
    constructor(game){
      this.game = game;
      this.collisionRadius = 40;
      this.margin = this.collisionRadius * 2;
      this.collisionX = this.margin + (Math.random() * (this.game.width - this.margin * 2));
      this.collisionY = this.game.topMagin + (Math.random() * (this.game.height - this.game.topMagin - this.margin));
      this.image = document.getElementById('egg');
      this.spriteWidth = 110;
      this.spriteHeight = 135;
      this.width = this.spriteWidth;
      this.height = this.spriteHeight;
      this.spriteX;
      this.spriteY; 
      this.hatchTimer = 0;
      this.hatchInterval = 10000;
      this.markedForDeletion = false;
    }
    draw(context){
      context.drawImage(this.image, this.spriteX, this.spriteY);
      if (this.game.debug){
        context.beginPath();
        context.arc(this.collisionX, this.collisionY, this.collisionRadius, 0, Math.PI * 2);
        context.save();
        context.globalAlpha = 0.5;
        context.fill();
        context.restore();
        context.stroke();
        const displayTimer = (this.hatchTimer * 0.001).toFixed(0); // just seconds // toFixed(0): passed number to string with 0 after decilal point
        context.fillText(displayTimer, this.collisionX, this.collisionY - this.collisionRadius * 2.5);
      }
    }
    // Game Rule: player must be able to push eggs outside the scene
    update(deltaTime){
      this.spriteX = this.collisionX - this.width * 0.5;
      this.spriteY = this.collisionY - this.height * 0.5 - 30; 
      // temporary helper
      let collisionObjects = [this.game.player, 
      ...this.game.obstacles, ...this.game.enemies]; // use SPREAD OPERATOR(...), to allow us to quicly expends our elements in an array into another array
      collisionObjects.forEach(object => {
        let [collision, distance, sumOfRadii, dx, dy] = this.game.checkCollision(this, object);
        // if collision is detected, we calculate in witch direction we wanna to push the egg
        if (collision){
          const unit_x = dx / distance;
          const unit_y = dy / distance;
          this.collisionX = object.collisionX + (sumOfRadii + 1) * unit_x;
          this.collisionY = object.collisionY + (sumOfRadii + 1) * unit_y;
        }
      });
      // Egg hatching 
      if (this.hatchTimer > this.hatchInterval || this.collisionY < this.game.topMagin){
        this.game.hatchlings.push(new Larva(this.game, this.collisionX, this.collisionY)); // push new larva to array (see the empty array into Game constructor)
        this.markedForDeletion = true;
        this.game.removeGameObjects();
        // console.log(this.game.eggs);
      } else {
        this.hatchTimer += deltaTime;
      }
    }
  }

  class Larva {
    constructor(game, x, y){
      this.game = game;
      this.collisionX = x;
      this.collisionY = y;
      this.collisionRadius = 30;
      this.image = document.getElementById('larva');
      this.spriteWidth = 150;
      this.spriteHeight = 150;
      this.width = this.spriteWidth;
      this.height = this.spriteHeight;
      this.spriteX;
      this.spriteY;
      this.speedY = 1 + Math.random();
      this.frameX = 0;
      this.frameY = Math.floor(Math.random() * 2);
    }
    draw(context){
      context.drawImage(
        this.image, 
        this.frameX * this.spriteWidth, 
        this.frameY * this.spriteHeight, 
        this.spriteWidth, 
        this.spriteHeight, 
        this.spriteX, 
        this.spriteY, 
        this.width,
        this.height
        );
      if (this.game.debug){
        context.beginPath();
        context.arc(this.collisionX, this.collisionY, this.collisionRadius, 0, Math.PI * 2);
        context.save();
        context.globalAlpha = 0.5;
        context.fill();
        context.restore();
        context.stroke();
      }
    }
    update(){
      this.collisionY -= this.speedY;
      this.spriteX = this.collisionX - this.width * 0.5;
      this.spriteY = this.collisionY - this.height * 0.5 - 50;
      // move to safety
      if (this.collisionY < this.game.topMagin){
        this.markedForDeletion = true;
        this.game.removeGameObjects();
        this.game.score++;
        for (let i = 0; i < 5; i++){
          this.game.particles.push(new Firefly    (this.game, this.collisionX, this.collisionY, 'orange'
          ));
        }
      }
      // collision with objects // check collisions method from Egg object, "collisionObjects"
      let collisionObjects = [this.game.player, 
      ...this.game.obstacles]; // use SPREAD OPERATOR(...), to allow us to quicly expends our elements in an array into another array
      collisionObjects.forEach(object => {
        let [collision, distance, sumOfRadii, dx, dy] = this.game.checkCollision(this, object);
        // if collision is detected, we calculate in witch direction we wanna to push the egg
        if (collision){
          const unit_x = dx / distance;
          const unit_y = dy / distance;
          this.collisionX = object.collisionX + (sumOfRadii + 1) * unit_x;
          this.collisionY = object.collisionY + (sumOfRadii + 1) * unit_y;
        }
      });
      // collision with enemies
      this.game.enemies.forEach(enemy => { // so, just wanna say that for each enemy, call this callback function
        if (this.game.checkCollision(this, enemy)[0]){
          this.markedForDeletion = true;
          this.game.removeGameObjects(); // filter larva out of array
          this.game.lostHatchinglings++;
          for (let i = 0; i < 5; i++){
            this.game.particles.push(new Spark(this.game, this.collisionX, this.collisionY, 'yellow'));
          } 
        }
      }); 
    }
  }

  class Enemy {
    constructor(game){
      this.game = game;
      this.collisionRadius = 30;
      // this.speedX = Math.random() * 3 + 0.5; // enemies speed defilement
      this.speedX = Math.random() * 3 + 2;
      // this.image = document.getElementById('toad');
      this.image = document.getElementById('toads');
      this.spriteWidth = 140;
      this.spriteHeight = 260;
      this.width = this.spriteWidth;
      this.height = this.spriteHeight;
      this.collisionX = this.game.width + this.width + Math.random() * this.game.width * 0.5;
      this.collisionY = this.game.topMagin + (Math.random() * (this.game.height - this.game.topMagin));
      this.spriteX;
      this.spriteY;
      this.frameX = 0;
      this.frameY = Math.floor(Math.random() * 4); // O is the first img, 1 the second, 2 third, 3 fourth, so random selected img
    }
    draw(context){
      context.drawImage(
        this.image, 
        this.frameX * this.spriteWidth, // sx, (source x), just 1 column, so it stay at 0
        this.frameY * this.spriteHeight, // sy, (source y) // replace this hard code value by "frameY" to random img     
        this.spriteWidth, // sw, // source width  
        this.spriteHeight, // sh, // source hight 
        // to specifie witch rectangle area we  define from the frame img
        this.spriteX, 
        this.spriteY, 
        this.width,// destination width
        this.height// destination hight
        // to define where we put that croped out pieced of img, on destination canvas
        );
      if (this.game.debug){
        context.beginPath();
        context.arc(this.collisionX, this.collisionY, this.collisionRadius, 0, Math.PI * 2);
        context.save();
        context.globalAlpha = 0.5;
        context.fill();
        context.restore();
        context.stroke();
      }
    }
    update(){      
      this.spriteX = this.collisionX - this.width + 40;
      this.spriteY = this.collisionY - this.height * 0.5;
      this.collisionX -= this.speedX;
      if (this.spriteX + this.width < 0){
        this.collisionX = this.game.width + this.width + Math.random() * this.game.width * 0.5;
        this.collisionY = this.game.topMagin + (Math.random() * (this.game.height - this.game.topMagin));
        this.frameY = Math.floor(Math.random() * 4);
      }
      let collisionObjects = [this.game.player, 
      ...this.game.obstacles];
      collisionObjects.forEach(object => {
        let [collision, distance, sumOfRadii, dx, dy] = this.game.checkCollision(this, object);
        if (collision){
          const unit_x = dx / distance;
          const unit_y = dy / distance;
          this.collisionX = object.collisionX + (sumOfRadii + 1) * unit_x;
          this.collisionY = object.collisionY + (sumOfRadii + 1) * unit_y;
        }
      });
    }
  }

  class Particle {
    constructor(game, x, y, color){
      this.game = game;
      this.collisionX = x;
      this.collisionY = y;
      this.color = color;
      this.radius = Math.floor(Math.random() * 10 + 5);
      this.speedX = Math.random() * 6 - 3;
      this.speedY = Math.random() * 2 + 0.5;
      this.angle = 0;
      this.va = Math.random() * 0.1 + 0.01;
      this.markedForDeletion = false;
    }
    draw(context){
      context.save();
      context.fillStyle = this.color;
      context.beginPath();
      context.arc(this.collisionX, this.collisionY, this.radius, 0, Math.PI * 2);
      context.fill();
      context.stroke();
      context.restore();
    }
  }

  class Firefly extends Particle {
    update(){
      this.angle += this.va;
      this.collisionX += Math.cos(this.angle) * this.speedX;
      this.collisionY -= this.speedY;
      if (this.collisionY < 0 - this.radius){
        this.markedForDeletion = true;
        this.game.removeGameObjects();
      }
    }
  }

  class Spark extends Particle {
    update(){
      this.angle += this.va * 0.5;
      this.collisionX -= Math.cos(this.angle) * this.speedX;
      this.collisionY -= Math.sin(this.angle) * this.speedY;
      if (this.radius > 0.1) this.radius -= 0.05;
      if (this.radius < 0.2){
        this.markedForDeletion = true;
        this.game.removeGameObjects();
      }
    }
  }

  class Game {
    constructor(canvas){
      this.canvas = canvas;
      this.width = this.canvas.width;
      this.height = this.canvas.height;
      this.player = new Player(this);
      this.fps = 70;
      this.timer = 0;
      this.interval = 1000/this.fps;
      this.eggTimer = 0;
      this.eggInterval = 1000;
      this.topMagin = 260;
      this.debug = true;
      this.numberOfObstacles = 7;
      this.maxEggs = 5;
      this.eggs = [];
      this.enemies = [];
      this.obstacles = [];
      this.gameObjects = [];
      this.particles = [];
      this.score = 0;
      this.winningScore = 5;
      this.lostHatchinglings = 0;
      this.hatchlings = []; // to push new larva into this array
      this.mouse = {
        x: this.width * 0.5,
        y: this.height * 0.5,
        pressed: false
      }
      canvas.addEventListener("mousedown", e => {
        this.mouse.x = e.offsetX;
        this.mouse.y = e.offsetY;
        this.mouse.pressed = true;
      })
      canvas.addEventListener("mouseup", e => {
        this.mouse.x = e.offsetX;
        this.mouse.y = e.offsetY;
        this.mouse.pressed = false;
      });
      canvas.addEventListener("mousemove", e => {
        if (this.mouse.pressed){
          this.mouse.x = e.offsetX;
          this.mouse.y = e.offsetY;
        }      
      });
      window.addEventListener("keydown", e => {
        // console.log(e);
        if (e.key == 'd') this.debug = !this.debug;
        // console.log(this.debug);
      });
    }
    render(context, deltaTime){
      if (this.timer > this.interval){
        context.clearRect(0, 0, this.width, this.height);
        this.gameObjects = [this.player, ...this.eggs, ...this.obstacles, ...this.enemies, ...this.hatchlings, ...this.particles];
        // sort by vertical position
        this.gameObjects.sort((a, b) => {
          return a.collisionY - b.collisionY;
        });
        this.gameObjects.forEach(object => {
          object.draw(context);
          object.update(deltaTime);
        });

        this.timer = 0;
      }
      this.timer += deltaTime;

      // add eggs periodically
      if (this.eggTimer > this.eggInterval && this.eggs.length < this.maxEggs){
        this.addEgg();
        this.eggTimer = 0;
        // console.log(this.eggs);
      } else {
        this.eggTimer += deltaTime;
      }

      // draw status text
      context.save(); 
      context.textAlign = 'left';
      context.fillText('Score : ' + this.score, 20, 100);
      if (this.debug){
        context.fillText('Lost : ' + this.lostHatchinglings, 20, 140);
      }
      context.restore(); // method to reset all canvas setting, before save() was called

      // draw game info.
      context.save();
      context.textAlign = 'right';
      ctx.font = '20px Helvetica';
      /* context.fillText("Action debug mode : press [d]", 1200, 80); */
      if (this.debug){
        context.fillText("Quit debug mode : press [d]", 1200, 80);
      } else {
        context.fillText("Action debug mode : press [d]", 1200, 80);
      }
      context.restore();

      // draw title
      context.save();
      context.textAlign = 'right';
      ctx.font = '13px Helvetica';
      if (this.debug){
        context.fillText('DEBUG or STRAIGHT TO HELL', 1200, 60);
      } else {
        context.fillText("PLAY (but you really have nothing else to do?)", 1200, 60);
      }
      context.restore();
    }
    checkCollision(a, b){
      const dx = a.collisionX - b.collisionX;
      const dy = a.collisionY - b.collisionY;
      const distance = Math.hypot(dy, dx);
      const sumOfRadii = a.collisionRadius + b.collisionRadius;
      return [(distance < sumOfRadii), // true if there is collision, false if ther's not
        distance, sumOfRadii, dx, dy]; 
    }
    addEgg(){
      this.eggs.push(new Egg(this));
    }
    addEnemy(){
      this.enemies.push(new Enemy(this));
    }
    removeGameObjects(){
      this.eggs = this.eggs.filter(object => !object.markedForDeletion);
      this.hatchlings = this.hatchlings.filter(object => !object.markedForDeletion);
      this.particles = this.particles.filter(object => !object.markedForDeletion);
      // console.log(this.gameObjects);
    }
    init(){ 
      for (let i = 0; i < 5; i++){
        this.addEnemy();
        // console.log(this.enemies);
      }
      let attempts = 0;
      while ((this.obstacles.length < this.numberOfObstacles) && (attempts < 500)){
        let testObstacle = new Obstacle(this);
        // console.log(testObstacle);
        let overlap = false;
        this.obstacles.forEach(obstacle => {
          const dx = testObstacle.collisionX - obstacle.collisionX;
          const dy = testObstacle.collisionY - obstacle.collisionY;
          const distance = Math.hypot(dy, dx);
          const distanceBuffer = 150;
          const sumOfRadii = testObstacle.collisionRadius + obstacle.collisionRadius + distanceBuffer;
          if (distance < sumOfRadii){
            overlap = true;
          }
        });
        const margin = testObstacle.collisionRadius * 2;
        if (!overlap && testObstacle.spriteX > 0 
            && testObstacle.spriteX < this.width - testObstacle.width 
            && testObstacle.collisionY > this.topMagin + margin
            && testObstacle.collisionY < this.height - margin
            ){
          this.obstacles.push(testObstacle);
        }
        attempts++;
      }
    }
  }

  const game = new Game(canvas);
  game.init();
  // console.log(game);

  // DELTA TIME: the amount of milliseconds that passed between each call of requestAnimationFrame
  // Wanna calculate the delta time
  let lastTime = 0;
  function animate(timeStamp){
    const deltaTime = timeStamp - lastTime;
    // deltaTime : is the difference between the timestamp from this animation loop & timestamp from previous animation loop
    lastTime = timeStamp;
    // console.log(deltaTime); // 16.6
    // ctx.clearRect(0, 0, canvas.width, canvas.height);
    game.render(ctx, deltaTime);
    window.requestAnimationFrame(animate);
    // requestAnimationFrame() -will autommatically try to adjust itself to the screen refrech rate, in most cases 60 frames per second                 -will auto. gernerate a timestamp, & pass timestamp as an arguments to the function it calls
  }
  animate(0); // 0 to auto-generated timestamp first value (no NAN)
});