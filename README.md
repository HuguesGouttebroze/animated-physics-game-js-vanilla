# Monsters GAME / HTML canvas / Sprites / JavaScript vanilla / no frameworks || no librairies

## Game rules
### Debug mode
+ this game have a debug mode, by default, that show circles objects
+ this debug game, by default, is on true when the game is loading
+ to toggle on debug mode just press touch `d`


## Code : memo. / values / definitions / ref.

+ `deltaTime` containes the number of milli seconds that happened bettween an animation frames & the previous animation frames.
    - `hatchTimer` is accumulating this `deltaTime` milliseconds between frames,      - if `hatchTimer` is more than `hatchInterval`, we'll deleted the egg & we'll replaced it with the larva.

```js
class Enemy {
    constructor(game){
      this.game = game;
      this.collisionRadius = 30;
      // this.speedX = Math.random() * 3 + 0.5; // enemies speed defilement
      this.speedX = Math.random() * 3 + 5;
      // this.image = document.getElementById('toad');
      this.image = document.getElementById('toads');
      this.spriteWidth = 140;
      this.spriteHeight = 260;
      this.width = this.spriteWidth;
      this.height = this.spriteHeight;
      this.collisionX = this.game.width + this.width + Math.random() * this.game.width * 0.5;
      this.collisionY = this.game.topMagin + (Math.random() * (this.game.height - this.game.topMagin));
      this.spriteX;
      this.spriteY;
      this.frameX = 0;
      this.frameY = 0;
    }
    draw(context){
      context.drawImage(
        this.image, 
        0, // sx, (source x), just 1 column, so it stay at 0
        this.frameY * this.spriteHeight, // sy, (source y) // O is the first img, 1 is the second, 2 is the third, 3 is fourth ... * this.spriteHeight, & replace this hard code value by "frameY" to random img     
        this.spriteWidth, // sw, // source width  
        this.spriteHeight, // sh, // source hight 
        // to specifie witch rectangle area we  define from the frame img
        this.spriteX, 
        this.spriteY, 
        this.width,// destination width
        this.height// destination hight
        // to define where we put that croped out pieced of img, on destination canvas
        );
      if (this.game.debug){
        context.beginPath();
        context.arc(this.collisionX, this.collisionY, this.collisionRadius, 0, Math.PI * 2);
        context.save();
        context.globalAlpha = 0.5;
        context.fill();
        context.restore();
        context.stroke();
      }
    }
    update(){      
      this.spriteX = this.collisionX - this.width + 40;
      this.spriteY = this.collisionY - this.height * 0.5;
      this.collisionX -= this.speedX;
      if (this.spriteX + this.width < 0){
        this.collisionX = this.game.width + this.width + Math.random() * this.game.width * 0.5;
        this.collisionY = this.game.topMagin + (Math.random() * (this.game.height - this.game.topMagin));
      }
      let collisionObjects = [this.game.player, 
      ...this.game.obstacles];
      collisionObjects.forEach(object => {
        let [collision, distance, sumOfRadii, dx, dy] = this.game.checkCollision(this, object);
        if (collision){
          const unit_x = dx / distance;
          const unit_y = dy / distance;
          this.collisionX = object.collisionX + (sumOfRadii + 1) * unit_x;
          this.collisionY = object.collisionY + (sumOfRadii + 1) * unit_y;
        }
      });
    }
  }

```